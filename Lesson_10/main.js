const help = {
  on: function (container, eventName, selector, handler) {
    container.addEventListener(
      eventName,
      function (e) {
        for (
          var target = e.target;
          target && target != this;
          target = target.parentNode
        ) {
          if (target.matches(selector)) {
            handler.call(target, e);
            break;
          }
        }
      },
      false
    );
  },
};

class TodosService {
  constructor() {
    this.todos = [];
    this.todosToShow = [];
    this.apiUrl = "https://jsonplaceholder.typicode.com";
  }

  getTodos(cb) {
    axios.get(`${this.apiUrl}/todos`).then((res) => {
      this.todos = res.data;
      cb(this.todos);
    });
  }

  addTodo(title, cb) {
    const newTodo = {
      title,
      completed: false,
    };
    axios.post(`${this.apiUrl}/todos`, newTodo).then((res) => {
      this.todos = [res.data, ...this.todos];
      cb(this.todos);
    });
  }

  deleteTodo(id, cb) {
    axios.delete(`${this.apiUrl}/todos/${id}`).then((res) => {
      this.todos = this.todos.filter((todo) => todo.id !== id);
      cb(this.todos);
    });
  }

  switchDone(id, cb) {
    this.todos = this.todos.map((todo) =>
      todo.id === id ? { ...todo, completed: !todo.completed } : todo
    );
    cb(this.todos);
  }

  filterByCompleted(completed = "all", cb) {
    switch (completed) {
      case "completed":
        cb(this.todos.filter((todo) => todo.completed));
        break;
      case "toDo":
        cb(this.todos.filter((todo) => !todo.completed));
        break;
      default:
        cb(this.todos);
        break;
    }
  }
}

class TodosTemplate {
  constructor(host) {
    this.host = host;
    this.formElement = this.host.querySelector(".todos-form");
    this.fieldElement = this.formElement.querySelector(".todos-form__field");
    this.listElement = this.host.querySelector(".todos-list");
    this.filtersElement = this.host.querySelector(".todos-filter");
    this.filterItemSelector = ".todos-filter__radio";
  }

  cleanInput() {
    this.fieldElement.value = "";
  }

  listenAdding(cb) {
    this.formElement.addEventListener("submit", (event) => {
      event.preventDefault();
      const title = this.fieldElement.value;
      if (title.length === 0) {
        alert("Введите title");
        return;
      }

      cb(title);
    });
  }

  listenDeleting(cb) {
    this.listElement.addEventListener("click", (event) => {
      try {
        const target = event.target;
        if (!target.dataset.action || target.dataset.action !== "delete-todo") {
          return;
        }
        const idToDelete = +target.dataset.todoId;

        if (!idToDelete || isNaN(idToDelete)) {
          throw "data-todoId is not attached to the button element or it is NaN";
        }
        cb(idToDelete);
      } catch (error) {
        console.error(error);
      }
    });
  }

  listenSwitchDone(cb) {
    help.on(
      this.listElement,
      "change",
      '[data-action="switch-todo-done"]',
      (event) => {
        try {
          const target = event.target;
          const id = +target.dataset.todoId;

          if (!id || isNaN(id)) {
            throw "data-todoId is not attached to the button element or it is NaN";
          }
          cb(id);
        } catch (error) {
          console.error(error);
        }
      }
    );
  }

  listenFiltering(cb) {
    help.on(this.filtersElement, "change", this.filterItemSelector, (event) => {
      const target = event.target;
      const filter = target.value;
      cb(filter);
    });
  }

  checkCompletedFilter(filter) {
    this.filtersElement
      .querySelectorAll(this.filterItemSelector)
      .forEach((filterEl) => {
        filterEl.checked = filterEl.value === filter ? true : null;
      });
  }

  renderList(todos) {
    let template = "";
    todos.forEach((todo) => {
      template += `
        <li class="todos-list__item todo">
          <input data-action="switch-todo-done" data-todo-id="${
            todo.id
          }" type="checkbox" ${todo.completed ? "checked" : ""}/>
          <span>${todo.title}</span>
          <button class="todo__delete" data-action="delete-todo" data-todo-id="${
            todo.id
          }">Delete</button>
        </li>
      `;
    });
    this.listElement.innerHTML = template;
  }
}

class TodosComponent {
  constructor(selector) {
    this.host = document.querySelector(selector);
    this.todosService = new TodosService();
    this.template = new TodosTemplate(this.host);
    this.init();
  }

  init() {
    const INITIAL_COMPLETED_FILTER = "completed";
    this.todosService.getTodos(
      function (todos) {
        this.todosService.filterByCompleted(
          INITIAL_COMPLETED_FILTER,
          function (todos) {
            console.log(todos);
            this.template.checkCompletedFilter(INITIAL_COMPLETED_FILTER);
            this.render(todos);
          }.bind(this)
        );
      }.bind(this)
    );
    this.listen();
  }

  listen() {
    this.template.listenAdding(this.addTodo.bind(this));
    this.template.listenDeleting(this.deleteTodo.bind(this));
    this.template.listenSwitchDone(this.switchDone.bind(this));
    this.template.listenFiltering(this.filterByCompleted.bind(this));
  }

  addTodo(title) {
    this.todosService.addTodo(
      title,
      function (todos) {
        this.render(todos);
        this.template.cleanInput();
      }.bind(this)
    );
  }

  deleteTodo(id) {
    this.todosService.deleteTodo(id, this.render.bind(this));
  }

  switchDone(id) {
    this.todosService.switchDone(id, this.render.bind(this));
  }

  filterByCompleted(completedFilter) {
    this.todosService.filterByCompleted(
      completedFilter,
      function (todos) {
        this.renderFiltered(completedFilter, todos);
      }.bind(this)
    );
  }

  renderFiltered(completedFilter, todos) {
    this.template.checkCompletedFilter(completedFilter);
    this.render(todos);
  }

  render(todos) {
    this.template.renderList(todos);
  }
}

const todosComponent = new TodosComponent("#todosComponent");
