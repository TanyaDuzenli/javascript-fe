class TodosModel {
    constructor() {
      this.todos = [];
      this.apiUrl = "https://jsonplaceholder.typicode.com";
    }
  
    getTodos(cb) {
      axios.get(`${this.apiUrl}/todos`).then((res) => {
        this.todos = res.data;
        console.log(this.todos);
        cb(this.todos);
      });
      // setTimeout(() => {
      //   cb(this.todos);
      // }, 1000);
    }
  
    addTodo(title, cb) {
      const newTodo = {
        title,
        done: false,
      };
      // make post request on https://jsonplaceholder.typicode.com/todos and handle response
      setTimeout(() => {
        this.todos = [newTodo, ...this.todos];
        cb(this.todos);
      }, 1000);
    }
  
    removeTodo(id, cb) {
      this.todos = this.todos.filter((todo) => todo.id !== id);
      cb(this.todos);
    }
  }
  
  class TodosView {
    constructor(host) {
      this.host = host;
      this.formElement = this.host.querySelector(".todos-form");
      this.fieldElement = this.formElement.querySelector(".todos-form__field");
      this.listElement = this.host.querySelector(".todos-list");
      this.removeBtnSelector = ".todo__remove";
    }
  
    clearInput() {
      this.fieldElement.value = "";
    }
  
    listenAdding(cb) {
      this.formElement.addEventListener("submit", (event) => {
        event.preventDefault();
        const title = this.fieldElement.value;
        if (title.length === 0) {
          alert("Введите title");
          return;
        }
        cb(title);
      });
    }
  
    listenRemoving(cb) {
      this.listElement.addEventListener("click", (event) => {
        const target = event.target;
        if (target.matches(this.removeBtnSelector)) {
          if (target.dataset.todoId) {
            cb(+target.dataset.todoId);
          }
        }
      });
    }
  
    renderList(todos) {
      let template = "";
      todos.forEach((todo) => {
        template += `
          <li class="todos-list__item todo">
            <button class="todo__remove" data-todo-id="${todo.id}">X</button>
            <span>${todo.title}</span>
            <input data-todo-id="${
              todo.id
            }" class="todo__change-status" type="checkbox" ${
          todo.completed ? "checked" : ""
        }/>
          </li>
        `;
      });
      this.listElement.innerHTML = template;
    }
  }
  class TodosController {
    constructor(selector) {
      this.host = document.querySelector(selector);
      this.model = new TodosModel();
      this.view = new TodosView(this.host);
      this.init();
    }
  
    init() {
      this.model.getTodos(this.render.bind(this));
  
      this.listen();
    }
  
    listen() {
      this.view.listenAdding(this.addTodo.bind(this));
      this.view.listenRemoving(this.removeTodo.bind(this));
    }
  
    render(todos) {
      this.view.renderList(todos);
    }
  
    addTodo(title) {
      this.model.addTodo(
        title,
        function (todos) {
          this.render(todos);
          this.view.clearInput();
        }.bind(this)
      );
    }
  
    removeTodo(id) {
      this.model.removeTodo(id, this.render.bind(this));
    }
  }
  
  const todosController = new TodosController("#todosComponent");
  
  