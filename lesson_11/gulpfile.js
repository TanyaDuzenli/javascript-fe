const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();
const webpack = require("webpack-stream");
const named = require("vinyl-named");

function style() {
  return gulp.src("app/scss/**/*.scss").pipe(sass()).pipe(gulp.dest("app/css"));
}

function js() {
  return gulp
    .src("./app/js/app.js")
    .pipe(named())
    .pipe(
      webpack({
        output: {
          filename: "[name].js",
        },
      })
    )
    .pipe(gulp.dest("./app"));
}

function server() {
  browserSync.init({
    server: {
      baseDir: "./app",
    },
  });
}

function watch() {
  gulp
    .watch("./app/scss/*.scss")
    .on("change", gulp.series(style, browserSync.reload));//сначала нужно компилировать style потом перезагрузить браузер
  gulp.watch("./app/*.html").on("change", browserSync.reload);
  gulp
    .watch("./app/js/**/*.js")
    .on("change", gulp.series(js, browserSync.reload));
}

// exports.style = style;
// exports.server = server;
// exports.watch = watch;
// exports.js = js;
exports.default = gulp.series(
  gulp.parallel(style, js),
  gulp.parallel(server, watch)
);
