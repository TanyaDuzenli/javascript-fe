class TodosComponent {
    constructor(selector) {
      
      this.host = document.querySelector(selector);
      this.formElement = this.host.querySelector(".todos-form");
      this.fieldElement = this.formElement.querySelector(".todos-form__field");
      this.listElement = this.host.querySelector(".todos-list");
      this.todos = [];
      this.renderList();
      this.listen();
    }
  
    addTodo(title) {
      const newTodo = {
          title: this.fieldElement.value,
          done: false,
    }
      this.todos = [...this.todos, newTodo];

      this.renderList();

};
  
    listen() {
      this.formElement.addEventListener("submit", (event) => {
        event.preventDefault();
        const title = this.fieldElement.value;
        if (title.length === 0) {
          alert("Введите дело");
          return;
        }
        this.addTodo(title);
        

      });
    };
  
    renderList() {
       let template = "";
     this.items.forEach(item => {
        template += `
        <div>${item.title}</div>
        `
    });
    this.renderList.innerHTML = template;
  };
  
}
const todosComponent = new TodosComponent("#todosComponent"); 