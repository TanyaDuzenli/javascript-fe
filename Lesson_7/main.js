/* Переписать корзину на класс и добавить функционал по изменению количества одинакового товара в корзине.

[] // init
[{id: 1, name: 'x', amount: 1}] // add
[{id: 1, name: 'x', amount: 2}] // add
[{id: 1, name: 'x', amount: 1}] // remove
[] // remove */


const products = [
    {
      id: 1,
      name: "iPhone 12",
      price: 800,
    },
    {
      id: 2,
      name: "AirPods",
      price: 200,
    },
    {
      id: 3,
      name: "Apple Watch",
      price: 600,
    },
    {
      id: 4,
      name: "MacBook PRO",
      price: 3000,
    },
  ];

  class Cart {
    constructor(product) {
    this.product = products;
    this.totalPrice;
    }
    add(product) {
      const isInCart = this.products.find(item => item.id === product.id);
      if (isInCart){
        this.products = this.products.map(item => item.id === product.id ? { ...item, amount: item.amount + 1} : { ...item });
      //добавляем amound
      } else {
        this.products = [...this.products, {...product, amount: 1}];  
      }
        this.calcPrice();
    }
    remove(idToRemove) {
      this.products.find(item => item.id === idToRemove);
      if (this.products.find (item => item.amount > 1)) {
      this.products = this.products.map(item => item.amount > 1 ? {...item, amount: item.amount - 1} : {...item});
      }
      else if (this.products.find (item => item.amount === 1)){
        this.products = this.products.filter(
          (product) => product.id !== idToRemove);
      }
    this.calcPrice();
    }
    calcPrice() {
      this.totalPrice = this.products.reduce((sum, curr) => (sum + curr.price * curr.amount ), 0)
      
    }
  }


cart.add(products[1]);
cart.add(products[1]);
cart.add(products[2]);
console.log(cart.products);

console.log(cart.products);
console.log(cart);

 
     
/* class Cart {
    constructor (products, totalPrice){
        this.products = products;
        this.totalPrice = totalPrice;
    }
    add(product) {
        this.products = [...this.products, product];
       this.calcPrice();
      }
      remove(idToRemove) {
        this.products = this.products.filter(
          (product) => product.id !== idToRemove
        );
        this.calcPrice();
      }
      calcPrice() {
        this.totalPrice = this.products.reduce((sum, curr) => (sum + curr.price), 0)
        
      }

}
   */
  
  
  