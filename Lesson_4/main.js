/* 1. Написать функцию map(fn, array), которая принимает
 на вход функцию и массив, и обрабатывает каждый элемент
  массива этой функцией, возвращая новый массив
   */
/* let arr = [1, 2, 3, 4];

const square = function (a) {
    return a * a;
};

function map (x, cb) {
    let arr2 = [];
    for (let i = 0; i < arr.length; i++) {
        const result = cb(arr[i]);
        arr2.push(result);
}
return arr2;
}

console.log(map (arr, square)); */

/* 2. Написать функцию pluck, которая берет массив объектов и возвращает массив значений определенного поля */

/* const characters = [
    { name: "barney", age: 36 },
    { name: "fred", age: 40 },
  ];

  function pluck (objects, field) {
  const arr = [];
  for (let i = 0; i < objects.length; i++) {
    arr.push(objects[i][field]);
  }
   return arr;
  }

  let result = pluck(characters, "name"); 
 */

 /*  3. Написать функцию создания генератора sequence(start, step). Она при вызове возвращает другую функцию-генератор,
   которая при каждом вызове дает число на 1 больше, и так до бесконечности. Начальное число, с которого начинать
    отсчет, и шаг, задается при создании генератора. Шаг можно не указывать, тогда он будет равен одному.
     Начальное значение по умолчанию равно 0. Генераторов можно создать сколько угодно.

 */

      function sequence(start = 0, step = 1) {
        let result = start;
        return function() {
          let result = start; //значение для вывода
          start = start + step;
          //start += step;
          return result;
        }
      }

      const generator = sequence(10, 3);
      const generator2 = sequence(7, 1);

      console.log(generator());
      console.log(generator());

      console.log(generator2());
 

     /*  4. Также, нужна функция take(gen, x) которая вызвает функцию gen заданное число (x) раз и 
      возвращает массив с результатами вызовов. Она нам пригодится для отладки */

      function sequence(start = 0, step = 1) {
        var result = start;
        return function () {
            result = start;
            start += step;
            return result;
        };
    };
    
    function take (gen, x) {
      let arr = [];
      for(let i = 0; i < x; i++)
          arr.push(gen());
          
      return arr;
    }
    
    console.log(take(sequence(0, 2), 5));
    console.log(take(sequence(0, 2), 6));
    console.log(take(sequence(0, 5), 4));