//Продолжая предидущее задание (спрашивать циклически память телефона, пока не введет корректную память). 
//У нас есть соответствие памяти к цене. Следовательно - это соответствие можно "понимать" не на уровне проверок 
//через switch ... case (if else), а на уровне хранения данных. Для начала - памяти можно хранить в массиве 
//(ПОДСКАЗКА: память - это не только объем, но и соответствующая цена за этот объем памяти). 
//Внешний цикл отвечает за "переспрашивание" памяти, а внтруенний за перебор всех памятей и "поиск" 
//соответствующей цены. (Сравнить память которую ввел пользователь с каждой из доступных памятей (массив какой-то))
//Сделать тоже самое для цвета. Соотношение цвет - картинка. Соответственно бдует массив с цветами



const memories = [
    {
        memory: 64,
        price: 700,
    },
    {
        memory: 128,
        price: 800, 
    },
    {
        memory: 256,
        price: 900,
    },
];
const colors = [
    {
        color: "black",
        image: "black1.png",
        surcharge: 10,

    },
    {
        color: "gold",
        image: "gold.png", 
        surcharge: 20, 
    },
    {
        color: "silver",
        image: "silver.png", 
        surcharge: 5, 
    },
];

let price;
let memory;
let surcharge;

outer: while (true) {
    memory = prompt ("Пожалуйста,введите обьем памяти вашего смартфона");
    for (let i = 0; i < memories.length; i++) {
    
    if (memories[i].memory === +memory) {
        price = memories[i].price;
        break outer;
    }
    else if (+memory === null) {
        alert ("До свидания!");
        break;
    }
  } 
}

outer: while (price) {
    color = prompt ("Пожалуйста,введите желаемый цвет");
    for (let j = 0; j < colors.length; j++) {

        if (colors[j].color === color) {
            image = colors[j].image;
            surcharge = colors[j].surcharge;
            break outer;
        }
        else if (color === null) {
            alert ("До свидания!");
            break;
        }
    }
}
if (price) {
    document.write("<h1>Стоимость: " + price + "$</h1>");
}

if (image) {
    document.write('<img src="./img/' + image + '">');
    document.write("<h1>Доплата за цвет: " + surcharge + "$</h1>");
 }
