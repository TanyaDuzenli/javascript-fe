export const help = {
  on: function (container, eventName, selector, handler) {
    container.addEventListener(
      eventName,
      function (e) {
        for (
          var target = e.target;
          target && target != this;
          target = target.parentNode
        ) {
          if (target.matches(selector)) {
            handler.call(target, e);
            break;
          }
        }
      },
      false
    );
  },
};


export class TodosView {
    constructor(host) {
      this.host = host;
      this.formElement = this.host.querySelector(".todos-form");
      this.fieldElement = this.formElement.querySelector(".todos-form__field");
      this.listElement = this.host.querySelector(".todos-list");
      this.filtersElement = this.host.querySelector(".todos-filter");
      this.filterItemSelector = ".todos-filter__radio";
    }
  
    cleanInput() {
      this.fieldElement.value = "";
    }
  
    listenAdding(cb) {
      this.formElement.addEventListener("submit", (event) => {
        event.preventDefault();
        const title = this.fieldElement.value;
        if (title.length === 0) {
          alert("Введите title");
          return;
        }
  
        cb(title);
      });
    }
  
    listenDeleting(cb) {
      this.listElement.addEventListener("click", (event) => {
        try {
          const target = event.target;
          if (!target.dataset.action || target.dataset.action !== "delete-todo") {
            return;
          }
          const idToDelete = +target.dataset.todoId;
  
          if (!idToDelete || isNaN(idToDelete)) {
            throw "data-todoId is not attached to the button element or it is NaN";
          }
          cb(idToDelete);
        } catch (error) {
          console.error(error);
        }
      });
    }
  
    listenSwitchDone(cb) {
      help.on(
        this.listElement,
        "change",
        '[data-action="switch-todo-done"]',
        (event) => {
          try {
            const target = event.target;
            const id = +target.dataset.todoId;
  
            if (!id || isNaN(id)) {
              throw "data-todoId is not attached to the button element or it is NaN";
            }
            cb(id);
          } catch (error) {
            console.error(error);
          }
        }
      );
    }
  
    listenFiltering(cb) {
      help.on(this.filtersElement, "change", this.filterItemSelector, (event) => {
        const target = event.target;
        const filter = target.value;
        cb(filter);
      });
    }
  
    checkCompletedFilter(filter) {
      this.filtersElement
        .querySelectorAll(this.filterItemSelector)
        .forEach((filterEl) => {
          filterEl.checked = filterEl.value === filter ? true : null;
        });
    }
  
    renderList(todos) {
      let template = "";
      todos.forEach((todo) => {
        template += `
          <li class="todos-list__item todo">
            <input data-action="switch-todo-done" data-todo-id="${
              todo.id
            }" type="checkbox" ${todo.completed ? "checked" : ""}/>
            <span>${todo.title}</span>
            <button class="todo__delete" data-action="delete-todo" data-todo-id="${
              todo.id
            }">Delete</button>
          </li>
        `;
      });
      this.listElement.innerHTML = template;
    }
  }