import {TodosModel} from "./Model.js";
 import {TodosView, help} from "./View.js";

export class TodosController {
    constructor(selector) {
      this.host = document.querySelector(selector);
      this.model= new TodosModel();
      this.view = new TodosView(this.host);
      this.init();
    }

    

  
    init() {
      const INITIAL_COMPLETED_FILTER = "completed";
      this.model.getTodos(
        function (todos) {
          this.model.filterByCompleted(
            INITIAL_COMPLETED_FILTER,
            function (todos) {
              console.log(todos);
              this.view.checkCompletedFilter(INITIAL_COMPLETED_FILTER);
              this.render(todos);
            }.bind(this)
          );
        }.bind(this)
      );
      this.listen();
    }
  
    listen() {
      this.view.listenAdding(this.addTodo.bind(this));
      this.view.listenDeleting(this.deleteTodo.bind(this));
      this.view.listenSwitchDone(this.switchDone.bind(this));
      this.view.listenFiltering(this.filterByCompleted.bind(this));
    }
  
    addTodo(title) {
      this.model.addTodo(
        title,
        function (todos) {
          this.render(todos);
          this.view.cleanInput();
        }.bind(this)
      );
    }
  
    deleteTodo(id) {
      this.model.deleteTodo(id, this.render.bind(this));
    }
  
    switchDone(id) {
      this.model.switchDone(id, this.render.bind(this));
    }
  
    filterByCompleted(completedFilter) {
      this.model.filterByCompleted(
        completedFilter,
        function (todos) {
          this.renderFiltered(completedFilter, todos);
        }.bind(this)
      );
    }
  
    renderFiltered(completedFilter, todos) {
      this.view.checkCompletedFilter(completedFilter);
      this.render(todos);
    }
  
    render(todos) {
      this.view.renderList(todos);
    }

    

  }
  
   