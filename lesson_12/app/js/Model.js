const axios = require("axios");
export class TodosModel {
  
    constructor() {
      this.todos = [];
      this.todosToShow = [];
      this.apiUrl = "https://jsonplaceholder.typicode.com";
      
    }
    
    getTodos(cb) {
      axios.get(`${this.apiUrl}/todos`).then((res) => {
        this.todos = res.data;
        cb(this.todos);
      });
    }
  
    addTodo(title, cb) {
      const newTodo = {
        title,
        completed: false,
      };
      axios.post(`${this.apiUrl}/todos`, newTodo).then((res) => {
        this.todos = [res.data, ...this.todos];
        cb(this.todos);
      });
    }
  
    deleteTodo(id, cb) {
      axios.delete(`${this.apiUrl}/todos/${id}`).then((res) => {
        this.todos = this.todos.filter((todo) => todo.id !== id);
        cb(this.todos);
      });
    }
  
    switchDone(id, cb) {
      this.todos = this.todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      );
      cb(this.todos);
    }
  
    filterByCompleted(completed = "all", cb) {
      switch (completed) {
        case "completed":
          cb(this.todos.filter((todo) => todo.completed));
          break;
        case "toDo":
          cb(this.todos.filter((todo) => !todo.completed));
          break;
        default:
          cb(this.todos);
          break;
      }
    }
  }